import React from 'react';
import PropTypes from 'prop-types';
import { AppBar, CssBaseline, Drawer, ListItem, ListItemText, Toolbar, Typography, withStyles } from 'material-ui';

import styles from './helpers/styles'
import Home from './components/Home'
import TableContainer from './containers/TableContainer'

class App extends React.Component {
  state = { page: 'api' }

  setPage = page => {
    this.setState({ page });
  }

  renderPage(page) {
    switch(page)
    {
      case 'api':
        return <TableContainer />
      default:
        return <Home />
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <div className="App">
        <CssBaseline />
        <div className={classes.root}>
          <AppBar position="absolute" className={classes.appBar}>
            <Toolbar>
              <Typography variant="title" color="inherit" noWrap>
                Test
              </Typography>
            </Toolbar>
          </AppBar>
          <Drawer
            variant="permanent"
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            <div className={classes.toolbar} />
            <ListItem button onClick={()=>{this.setPage('home')}}>
              <ListItemText primary="Home"/>
            </ListItem>
            <ListItem button onClick={()=>{this.setPage('api')}}>
              <ListItemText primary="API page"  />
            </ListItem>
          </Drawer>
          <main className={classes.content}>
            <div className={classes.toolbar} />
            {this.renderPage(this.state.page)}
          </main>
        </div>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);