export const fetchData = (uri, scope) => {
  fetch(uri)
    .then(response => response.json())
    .then(data => {
      return scope.setState({ data })
    } )
    .catch(err => console.log('error fetching from omdbapi:', err))
}
