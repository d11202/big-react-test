import React from 'react';
import Uppercase from './uppercase';
import TextField from 'material-ui/TextField';

class Home extends React.Component {
  state = { name: '' };

  handleNameChange = (event) => {
    this.setState({ name: event.target.value });
  }

  render() {
    return <div>
      <h3>Welcome to the BiG react test!</h3>
      <br />
      <p>Enter your name:</p>
      <TextField
        id="name"
        label="Name"
        value={this.state.name}
        onChange={this.handleNameChange}
        margin="normal"
      />
    <p>
      Name in uppercase: <Uppercase value={this.state.name} />
    </p>
    </div>
  }
}

export default Home;