import React from 'react';
import PropTypes from 'prop-types';

class Uppercase extends React.Component {
  state = {
    uppercaseValue: ''
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if(prevState.value !== nextProps.value)
    {
      return {
        uppercaseValue: nextProps.value.toUpperCase()
      }
    }
  }

  render() {
    return this.state.uppercaseValue;
  }
}

Uppercase.propTypes = {
  value: PropTypes.string.isRequired,
};

export default Uppercase;