import React, { PureComponent } from 'react'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  searchWrapper: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  searchbar: {
    minWidth: 300,
    width: '100%',
    marginRight: '1rem'
  },
  button: {
    margin: theme.spacing.unit,
    marginRight: 0,
    width: '8rem',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
})

class SearchBar extends PureComponent {

  handleClick = (event, media) => {
    console.log('handleClick', media.Title)
    // renderImage();
  }

  render() {
    const { classes, searchInput, makeApiRequest, setSearchInput } = this.props;

    return (
      <div className={classes.searchWrapper}>
        <TextField
            id="search"
            label="Search field"
            type="search"
            className={classes.searchbar}
            value={searchInput}
            onChange={setSearchInput}
            margin="normal"
          />
        <Button className={classes.button}
                variant="raised"
                color="primary"
                onClick={ makeApiRequest }>
          Search
        </Button>
      </div>
    )
  }
}

export default withStyles(styles)(SearchBar)