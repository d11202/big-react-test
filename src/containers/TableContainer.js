import React, { PureComponent, Fragment } from 'react'
import Paper from 'material-ui/Paper'
import Table, { TableBody, TableCell, TableHead, TableRow, TablePagination } from 'material-ui/Table'
import { withStyles } from 'material-ui/styles';
import { fetchData } from '../api/api'
import './TableContainer.css'
import SearchBar from './SearchBar';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    marginRight: '5rem',
    overflowX: 'auto',
    height: '100%'
  },
  table: {
    width: '100%',
  },
})

class TableContainer extends PureComponent {
    state = {
      data: { Search: [], Response: 'False' },
      searchInput: 'the original series',
      page: 0,
      rowsPerPage: 5,
      isPosterVisible: false,
      posterUrl: null,
      posterAlt: null,
    }

    getMovieListBySearchString() {
      /**
       * async call to API and sets response into data prop of component state
       */
      fetchData(`http://www.omdbapi.com/?apikey=7a52ba1f&s=${this.state.searchInput}`, this)
    }

    makeApiRequest = event => {
      if(this.state.searchInput.length > 2) {
        this.getMovieListBySearchString()
      }
    }

    setSearchInput = event => {
      this.setState({ searchInput: event.target.value })
    }

    viewImage = (event, media) => {
      this.setState({ isPosterVisible: !this.state.isPosterVisible,
                      posterUrl: media.Poster,
                      posterAlt: `Image of movie titled ${media.Title}` })
    }

    handleChangePage = (event, page) => {
      this.setState({ page })
    }

    handleChangeRowsPerPage = event => {
      this.setState({ rowsPerPage: event.target.value })
    }

    onHandleClose = () => {
      this.setState({
        isPosterVisible: false,
        posterUrl: null,
        posterAlt: null,
      })
    }

    renderTable = props => {
      const { classes } = this.props;
      const { data, rowsPerPage, page } = this.state;
      const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.Search.length - page * rowsPerPage);
      
      return (
        <div className='table__container'>
          <Paper className={classes.root} elevation={10}>
            { this.state.isPosterVisible &&
            <div className='table__poster'>{this.state.posterUrl === 'N/A' ? <span className='table__no-image'>No image available</span> : <img className='table__img' src={this.state.posterUrl} alt={this.state.posterAlt} />}<div className='table__img--close' onClick={this.onHandleClose}>X</div></div> }
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  {Object.keys(this.state.data.Search[0]).map((name, index) => <TableCell key={index}>{name === 'imdbID' ? 'imdb ID' : name}</TableCell>)}
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.data.Search.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(media => {
                  return (
                    <TableRow
                      key={media.imdbID}
                      hover
                      tabIndex={-1}
                      onClick={event => this.viewImage(event, media)}
                    >
                      <TableCell style={{ minWidth: '250px'}}>{media.Title}</TableCell>
                      <TableCell>{media.Year}</TableCell>
                      <TableCell>{media.imdbID}</TableCell>
                      <TableCell>{media.Type}</TableCell>
                      <TableCell>View image</TableCell>
                    </TableRow>
                  );
                })}
                { emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
                )}
              </TableBody>
            </Table>
            <TablePagination
              component="div"
              count={this.state.data.Search.length}
              rowsPerPage={this.state.rowsPerPage}
              page={this.state.page}
              backIconButtonProps={{
                'aria-label': 'Previous Page',
              }}
              nextIconButtonProps={{
                'aria-label': 'Next Page',
              }}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
            />
          </Paper>
        </div>
      )
    }

    render() {
      /**
       * the property Response is a string in Title Case. Converts 'True' to true, 'False' to false
       * @param isApiResponseValid: If true this.renderTable is called
       **/
      const isApiResponseValid = JSON.parse(this.state.data.Response.toLowerCase());
      return (
        <Fragment>
          <SearchBar makeApiRequest={this.makeApiRequest} setSearchInput={this.setSearchInput} searchInput={this.state.searchInput} />
          { isApiResponseValid && this.renderTable() }
        </Fragment>
      )
    }
}

export default withStyles(styles, { name: 'MuiPaper' })(TableContainer)